package main

import (
	"fmt"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"strconv"
)

type Slide func(nextSlide func()) (title string, content tview.Primitive)

var (
	BORDER bool = true
	app         = tview.NewApplication()
)

func main() {
	app := tview.NewApplication()

	mainflex := tview.NewFlex()
	mainflex.SetBorder(BORDER).SetTitle("main").SetRect(0, 0, 100, 80)
	commonlfex := tview.NewFlex()
	commonlfex.SetBorder(BORDER).SetTitle("common").SetRect(0, 0, 20, 70)

	tabflex := tview.NewFlex()
	tabflex.SetBorder(BORDER).SetTitle("tab").SetRect(0, 0, 70, 70)

	//pages

	slides := []Slide{
		TablePage,
		func(nextSlide func()) (title string, content tview.Primitive) {
			title = "page2"
			content = tview.NewBox().SetTitle(title).SetBorder(BORDER)
			return
		},
		func(nextSlide func()) (title string, content tview.Primitive) {
			title = "page3"
			content = tview.NewBox().SetTitle(title).SetBorder(BORDER)
			return
		},
	}

	pages := tview.NewPages()

	//layout management
	mainflex.AddItem(commonlfex, 40, 1, false)
	mainflex.AddItem(tabflex, 0, 4, false)

	//tab info: The bottom row has some info on where we are.
	info := tview.NewTextView().
		SetDynamicColors(true).
		SetRegions(true).
		SetWrap(false)

	currentSlide := 0
	info.Highlight(strconv.Itoa(currentSlide))

	slidex := func(x int) {
		x -= 1
		currentSlide = x
		info.Highlight(strconv.Itoa(currentSlide))
		pages.SwitchToPage(strconv.Itoa(currentSlide))

	}
	nextSlide := func() {
		currentSlide = (currentSlide + 1) % len(slides)
		info.Highlight(strconv.Itoa(currentSlide))
		pages.SwitchToPage(strconv.Itoa(currentSlide))
	}

	for index, slide := range slides {
		title, primitive := slide(nextSlide)
		pages.AddPage(strconv.Itoa(index), primitive, true, index == currentSlide)
		fmt.Fprintf(info, `%d ["%d"][darkcyan]%s[white][""]  `, index+1, index, title)
	}

	tabflex.SetDirection(tview.FlexRow)
	tabflex.AddItem(pages, 0, 1, true)
	tabflex.AddItem(info, 1, 1, true)

	// Shortcuts to navigate the slides.
	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyF1 {
			slidex(1)
		} else if event.Key() == tcell.KeyF2 {
			slidex(2)
		} else if event.Key() == tcell.KeyF3 {
			slidex(3)
		} else {
			//use switch case formally
		}
		return event
	})

	if err := app.SetRoot(mainflex, true).SetFocus(list).Run(); err != nil {
		panic(err)
	}

}
